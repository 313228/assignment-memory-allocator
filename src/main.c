#include <stdio.h>
#include <unistd.h>
#include <inttypes.h>
#include "mem.h"
#include "mem_internals.h"

int getpagesize();

void simple_malloc(int64_t* first, void* heap){
    printf("Simple allocation\n");
    printf("Before:\n");
    debug_heap(stdout, heap);
    printf("After:\n");
    first  = _malloc(sizeof(*first));
    debug_heap(stdout, heap);
    
    struct block_header const* header =  (struct block_header const*)heap;
    
    if (header->is_free) {
        printf("Test failed\n");
    }
    else {
        printf("Test went succesfully\n");
    }
    
    _free(first);
}

void freeing_1_from_many(int64_t* first, int64_t* second, int64_t* third, void* heap){
    printf("Freeing one block from many\n");
    first  = _malloc(sizeof(*first));
    second  = _malloc(sizeof(*second));
    third  = _malloc(sizeof(*third));
    printf("Before:\n");
    debug_heap(stdout, heap);
    printf("After:\n");
    _free(first);
    debug_heap(stdout, heap);
    
    struct block_header const* first_header =  (struct block_header const*)heap;
    struct block_header* second_header = first_header->next;
    struct block_header* third_header = second_header->next;
    
    if(!(first_header->is_free) || second_header->is_free || third_header->is_free) {
        printf("Test failed\n");
    }
    else {
        printf("Test went succesfully\n");
    }
    
    _free(second);
    _free(third);
}

void freeing_2_from_many(int64_t* first, int64_t* second, int64_t* third, void* heap){
    printf("Freeing two blocks from many\n");
    first  = _malloc(sizeof(*first));
    second  = _malloc(sizeof(*second));
    third  = _malloc(sizeof(*third));
    printf("Before:\n");
    debug_heap(stdout, heap);
    printf("After:\n");
    _free(first);
    _free(second);
    debug_heap(stdout, heap);
    
    struct block_header const* first_header =  (struct block_header const*)heap;
    struct block_header* second_header = first_header->next;
    struct block_header* third_header = second_header->next;
    
    if(!(first_header->is_free) || !(second_header->is_free) || third_header->is_free) {
        printf("Test failed\n");
    }
    else {
        printf("Test went succesfully\n");
    }
    
    _free(third);
}

void out_of_memory_expanding(int64_t* first, int64_t* second, void* heap){
    printf("Out of memory, expanding old region\n");
    first = _malloc(8174);
    printf("Before:\n");
    debug_heap(stdout, heap);
    printf("After:\n");
    second = _malloc(sizeof(*second));
    
    debug_heap(stdout, heap);
    
    _free(first);
    _free(second);
}

void out_of_memory_new_region(int64_t* first, int64_t* second, void* heap){
    printf("Out of memory, finding new region\n");
    first = _malloc(getpagesize() * 4);
    printf("Before:\n");
    debug_heap(stdout, heap);
    printf("After:\n");
    second = _malloc(getpagesize() * 8);
    debug_heap(stdout, heap);
    
    _free(first);
    _free(second);
}

int main() {
    void* heap = heap_init(1024);
    int64_t* first_block = NULL;
    int64_t* second_block = NULL;
    int64_t* third_block = NULL;
    
    simple_malloc(first_block, heap);

    freeing_1_from_many(first_block, second_block, third_block, heap);
    
    freeing_2_from_many(first_block, second_block, third_block, heap);
    
    out_of_memory_expanding(first_block, second_block, heap);
    
    out_of_memory_new_region(first_block, second_block, heap);
    return 0;
}
